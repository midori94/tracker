#!.venv/bin/python
from itertools import count
from models import *
from config import *
from utils import *
import cv2

with open(DETECTIONS_FILE_PATH) as f:
    detections = load_detections(f)

cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
cv2.namedWindow('detection', cv2.WINDOW_NORMAL)

frame_iter = count(1)
frame = get_frame(next(frame_iter))
while frame is not None:
    cv2.imshow('frame', frame)
    for _, pt1, pt2 in detections.pop(0):
        try:
            cropped_frame = crop_frame(frame, pt1, pt2)
            cv2.imshow('detection', cropped_frame)
            cv2.waitKey(0)
        except:
            pass


    frame = get_frame(next(frame_iter))




