#!.venv/bin/python
from configparser import ConfigParser

CONFIG_PATH = 'tracker.conf'

with open(CONFIG_PATH, 'r') as f:
    config_string = '[dummy_section]\n' + f.read()

config = ConfigParser()
config.read_string(config_string)
default = config['dummy_section']

INPUT_VIDEO_DIR = default.get('INPUT_VIDEO_DIR', 'data/test_video/img1/')
DETECTIONS_FILE_PATH = default.get('DETECTIONS_FILE_PATH', 'data/test_video/gt/gt.txt')
SCALER_MODEL_PATH = default.get('SCALER_MODEL_PATH', 'data/model.scaler')
PCA_MODEL_PATH = default.get('PCA_MODEL_PATH', 'data/model.pca')
