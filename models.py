#!.venv/bin/python  
from itertools import count

id_provider = count()
class Trajectory:
    
    def __init__(self):
        self.__id = next(id_provider)
        self.state = None

    def get_id(self):
        return self.__id


if __name__ == '__main__':
    trajectory1 = Trajectory()
    trajectory2 = Trajectory()

    print(trajectory1.id, trajectory2.id)
