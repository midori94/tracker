#!.venv/bin/python
from config import *
import cv2
import csv

def load_detections(f):
    csv_list = csv.reader(f, quoting = csv.QUOTE_NONNUMERIC)
    detections = []
    for frame_idx, det_idx, x, y, w, h, config, *_ in csv_list:
        if config:
            detection = (
                    int(det_idx),
                    (int(x), int(y)),
                    (int(x+w), int(y+h))
            )
            try:
                detections[int(frame_idx) - 1].append(detection)
            except:
                detections.append([detection])
    return detections

def get_frame(i):
    frame = cv2.imread(f'{INPUT_VIDEO_DIR}{i:06}.jpg')
    return frame

def crop_frame(frame, pt1, pt2):
    lower_limit = (
            max(pt1[1], 0),
            max(pt1[0], 0)
    )
    frame_shape = frame.shape
    upper_limit = (
            min(pt2[1], frame_shape[0] - 1),
            min(pt2[0], frame_shape[1] - 1)
    )

    cropped_frame = frame[
            lower_limit[0]:upper_limit[0],
            lower_limit[1]:upper_limit[1]
    ]
    return cropped_frame
            
if __name__ == '__main__':
    from itertools import count
    from config import *
    import numpy as np
    import cv2
    
    cv2.namedWindow('video', cv2.WINDOW_NORMAL)

    with open(DETECTIONS_FILE_PATH, 'r') as f:          
        detections = load_detections(f)

    i = count(1)
    frame = get_frame(next(i))
    while frame is not None:
        for idx, pt1, pt2 in detections.pop(0):
            cv2.rectangle(frame, pt1, pt2, (0,255,0), thickness = 3)
        cv2.imshow('video', frame)
        key = cv2.waitKey(10)
        if key == ord('q'):
            exit(0)

        frame = get_frame(next(i))
